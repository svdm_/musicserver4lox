<?php
//Czech
//General
$language['GENERAL_SAVE'] = 'Uložit';
$language['GENERAL_CLOSE'] = 'Zavřít';
$language['GENERAL_YES'] = 'Ano';
$language['GENERAL_NO'] = 'Ne';
$language['GENERAL_MANUAL'] = 'Manuální';
$language['GENERAL_FORUM'] = 'Loxforum';
$language['GENERAL_START'] = 'Start';
$language['GENERAL_STOP'] = 'Stop';
$language['GENERAL_RESTART'] = 'Restartovat';
$language['GENERAL_TEST'] = 'Test';
$language['GENERAL_SETTINGS'] = 'Nastavení';
$language['GENERAL_EQ'] = 'Equalizer';
$language['GENERAL_TT_START'] = 'Start služby';
$language['GENERAL_TT_STOP'] = 'Stop služby';
$language['GENERAL_TT_RESTART'] = 'Stop a restart';
$language['GENERAL_TT_TEST'] = 'Test zařízení';
$language['GENERAL_TT_CONTROLELEMENTS'] = 'Spusť kontrolu komponentů';
$language['GENERAL_TT_SERVICE_RUNNING'] = 'Služba běží';
$language['GENERAL_TT_SERVICE_STOPED'] = 'Služba je zastavena';
$language['GENERAL_TT_SERVICE_PLAYER_STANDBY'] = 'Služba běží - Přehrávač je ve standbay';
$language['GENERAL_TT_SERVICE_REACHABLE'] = 'Služba je dostupná';
$language['GENERAL_TT_SERVICE_UNREACHABLE'] = 'Služba není dostupná';
$language['GENERAL_TT_REACHABLE'] = 'Dostupný';
$language['GENERAL_TT_UNREACHABLE'] = 'Nedostupný';
$language['GENERAL_TT_SETTINGS'] = 'Nastavení';

//Languages
$language['GENERAL_LANG_DE'] = 'deutsch';
$language['GENERAL_LANG_EN'] = 'english';
$language['GENERAL_LANG_PL'] = 'polski';
$language['GENERAL_LANG_CZ'] = 'čeština';

// Power
$language['POWER_PAGE_TITLE'] = 'MS4L stav';
$language['POWER_SHUTDOWN'] = 'Vypnout systém';
$language['POWER_REBOOT'] = 'Restartovat system';
$language['POWER_MODAL_SHUTDOWN_TITLE'] = 'Vypnout system';
$language['POWER_MODAL_SHUTDOWN_MSG'] = 'Opravdu chcete vypnout systém?';
$language['POWER_MODAL_REBOOT_TITLE'] = 'Restartujte system';
$language['POWER_MODAL_REBOOT_MSG'] = 'Opravdu chcete restartovat system?';
$language['POWER_SHUTDOWN_MSG'] = 'Systém se vypíná…';
$language['POWER_REBOOT_MSG'] = 'System se restartuje…';
$language['POWER_SHUTDOWN_BACK'] = 'Zpět na přihlášení';

//Navigaion Main
$language['NAV_MAIN_DASHBOARD'] = 'Přehled';
$language['NAV_MAIN_SETTINGS'] = 'Nastavení';
$language['NAV_MAIN_SETTINGS_PLAYER'] = 'Zóny';
$language['NAV_MAIN_SETTINGS_PLAYER_INTERNAL'] = 'interní';
$language['NAV_MAIN_SETTINGS_PLAYER_EXTERNAL'] = 'externí';
$language['NAV_MAIN_SETTINGS_EQUALIZER'] = 'Equalizer';
$language['NAV_MAIN_SETTINGS_TTS'] = 'TTS/Alarm/Zvonek';
$language['NAV_MAIN_SETTINGS_SERVICES'] = 'Služby';
$language['NAV_MAIN_SETTINGS_SUB_TTS'] = 'TTS/CTS';
$language['NAV_MAIN_SETTINGS_SUB_T2T'] = 'Doprava2TTS (t2t)';
$language['NAV_MAIN_SETTINGS_SUB_W2T'] = 'Počasí2TTS (w2t)';
$language['NAV_MAIN_SETTINGS_SUB_RING'] = 'Zvonek/Alarm/Budík';
$language['NAV_MAIN_SETTINGS_SERVICES_SUB_PM'] = 'Správce napájení';
$language['NAV_MAIN_SETTINGS_SERVICES_SUB_SQ2LOX'] = 'Squeeze2Lox / MusicServerGateway';
$language['NAV_MAIN_SETTINGS_SERVICES_SUB_FM'] = 'FollowMe';
$language['NAV_MAIN_SETTINGS_SERVICES_SUB_FC'] = 'FritzCallMonitor';
$language['NAV_MAIN_SETTINGS_SERVICES_SUB_T5'] = 'T5 Receiver';
$language['NAV_MAIN_SETUP'] = 'Nastavení';
$language['NAV_MAIN_SETUP_SOUNDCARD'] = 'Zvuková karta';
$language['NAV_MAIN_SETUP_NETWORK'] = 'Síť';
$language['NAV_MAIN_SETUP_NETWORK_SETTINGS'] = 'Síťové nastavení';
$language['NAV_MAIN_SETUP_NETWORK_MOUNT'] = 'Netshare mount';
$language['NAV_MAIN_SETUP_EMAIL'] = 'eMail';
$language['NAV_MAIN_SETUP_STATISTICS'] = 'Statistiky';
$language['NAV_MAIN_SETUP_LMS'] = 'Logitech Media Server';
$language['NAV_MAIN_SETUP_MINISERVER'] = 'MiniServer';
$language["NAV_MAIN_SETUP_PLAYER"] = 'Zóny';
$language["NAV_MAIN_SETUP_PLAYER_INTERNAL"] = 'interní';
$language["NAV_MAIN_SETUP_PLAYER_EXTERNAL"] = 'externí';
$language['NAV_MAIN_TOOLS'] = 'Nástroje';
$language['NAV_MAIN_TOOLS_UPDATE'] = 'Aktualizace';
$language['NAV_MAIN_TOOLS_BACKUP'] = 'Záloha';
$language['NAV_MAIN_TOOLS_SERVERRESET'] = 'MS4L restart';
$language['NAV_MAIN_TOOLS_LOXHELPER'] = 'Lox-Config nápověda';
$language['NAV_MAIN_TOOLS_LOXHELPER_EVENTCREATOR'] = 'Tvorba událostí';
$language['NAV_MAIN_TOOLS_LOXHELPER_TEMPLATECREATOR'] = 'Tvorba šablon';
$language['NAV_MAIN_HELP'] = 'Pomoc';
$language['NAV_MAIN_HELP_MANUAL'] = 'Manual';
$language['NAV_MAIN_HELP_FORUM'] = 'LoxForum';
$language['NAV_MAIN_HELP_SUPPORT_DATA'] = 'Pomocná data';
$language["NAV_MAIN_HELP_SUPPORT_LOGVIEWER"] = 'Prohlížení logu';
$language['NAV_MAIN_HELP_ABOUT'] = 'O aplikaci';

//Naviagtion Header
$language['NAV_HEADER_TT_SET_LANGUAGE'] = 'Výběr jazyka';
$language['NAV_HEADER_TT_SET_USER'] = 'Uživatelské nastavení';
$language['NAV_HEADER_TT_UPDATE'] = 'Aktualizace';
$language['NAV_HEADER_TT_UPDATE_AVAILABLE'] = 'K dispozici je aktualizace';
$language['NAV_HEADER_TT_POWER'] = 'Nabídka napájení';
$language['NAV_HEADER_TT_LOGOUT'] = 'Odhlásit uživatele';

//Login-Page
$language['LOGIN_PAGE_TITLE'] = 'MusicServer4Lox přihlášení';
$language['LOGIN_FORM_TITLE'] = 'Přihlášení do MusicServer';
$language['LOGIN_FORM_USER'] = 'Uživatel jméno';
$language['LOGIN_FORM_PASS'] = 'Heslo';
$language['LOGIN_FORM_FORGOT_PASS'] = 'Zapoměl jsi heslo?';
$language['LOGIN_FORM_FORGOT_RESET'] = 'Resetovat';
$language['LOGIN_FORM_FORGOT_LOGIN'] = 'Přihlásit se';
$language['LOGIN_FORM_MODAL_TITLE'] = 'Uživatelské jméno / Heslo  je špatné';
$language['LOGIN_FORM_MODAL_MSG'] = 'Zdá se, že vaše uživatelské jméno nebo heslo není správné!<br><b>Prosím zkuste znovu…</b>';

//Password Reset
$language['PASSWDRESE_PAGE_TITLE'] = 'MusicServer4Lox reset hesla';
$language["PASSWDRESET_TITLE"] = 'Reset hesla';
$language["PASSWDRESET_TEXT"] = 'Pokud jste nakonfigurovali poštovní server, bude vám zaslán e-mail s novým heslem.'; 
$language["PASSWDRESET_RESET"] = 'Reset hesla nyní';
$language["PASSWDRESET_MODAL_TITLE_MAILOK"] = 'Odeslání emailu v pořádku.';
$language["PASSWDRESET_MODAL_TITLE_MAILNOK"] = 'Chyba odeslání emailu';
$language["PASSWDRESET_MODAL_MSG_MAILOK"] = 'E-mail byl úspěšně odeslán.';
$language["PASSWDRESET_MODAL_MSG_MAILNOK"] = 'E-mail nelze odeslat.<br>Mail-Server nakonfigurovat?';
$language["PASSWDRESET_MAIL_SUBJECT"] = 'MS4L - nové hello';
$language["PASSWDRESET_MAIL_TEXT"] = 'Ahoj {realname},<br><br>požádali jste o nové heslo do MusicServer4Lox.<br><br>Uživatelské jméno: {username} <br>Tvoje nové heslo je: {password} <br><br>Nyní se můžete přihlásit pomocí nového hesla.<br><br>MusicServer4Lox team';

//Dashboard (index.php)
$language['DASHBOARD_PAGE_TITLE'] = 'MS4L přehled';
$language['DASHBOARD_TITLE'] = 'Přehled';
$language['DASHBOARD_TITLE_MUSICSERVER'] = 'MuiscServer';
$language['DASHBOARD_TITLE_NETWORK'] = 'Sít';
$language['DASHBOARD_TITLE_SERVICES'] = 'Služby';
$language['DASHBOARD_TITLE_ZONES'] = 'Music-Zóny';
$language['DASHBOARD_UPTIME'] = 'Čas';
$language['DASHBOARD_DAY'] = 'Den(y)';
$language['DASHBOARD_TOTAL'] = 'Celkem';
$language['DASHBOARD_FREE'] = 'Nevyužito';
$language['DASHBOARD_USED'] = 'Využito';
$language['DASHBOARD_HARDWARE'] = 'Hardware';
$language['DASHBOARD_CPU'] = 'Procesor';
$language['DASHBOARD_RAM'] = 'Pamět';
$language['DASHBOARD_HDD'] = 'Pevný disk';
$language['DASHBOARD_OS'] = 'Operační System';
$language['DASHBOARD_HOSTNAME'] = 'Název hostitele';
$language['DASHBOARD_IP'] = 'IP Adresa';
$language['DASHBOARD_NETMASK'] = 'Maska sítě';
$language['DASHBOARD_GATEWAY'] = 'Výchozí brána';
$language['DASHBOARD_DNS'] = 'DNS Server';
$language['DASHBOARD_INET'] = 'Internet';
$language['DASHBOARD_MINISERVER'] = 'MiniServer';
$language['DASHBOARD_PACKAGES_LOSS']= 'ztracené pakety';
$language['DASHBOARD_MS'] = 'MusicServer';
$language['DASHBOARD_START_ALL'] = 'Spustit všechny služby';
$language['DASHBOARD_STOP_ALL'] = 'Zastavit všechny služby';
$language['DASHBOARD_RESTART_ALL'] = 'Restartovat všechny služby';
$language['DASHBOARD_LMS'] = 'Logitech Media Server';
$language['DASHBOARD_LMS_WEB'] = 'LMS WebUI';
$language['DASHBOARD_LMS_TELNET'] = 'LMS Telnet';
$language['DASHBOARD_PM'] = 'Správce napájení';
$language['DASHBOARD_SQ2LOX'] = 'Squeeze2lox / MS-Výchozí brána';
$language['DASHBOARD_FC'] = 'FritzCall-Monitor';
$language['DASHBOARD_T5'] = 'T5 Receiver';
$language['DASHBOARD_SERVER_NOT_REACHABLE'] = 'Server není dostupný';
$language['DASHBOARD_NO_INTERNAL_PLAYER'] = 'Žádní interní zóna';
$language['DASHBOARD_NO_EXTERNAL_PLAYER'] = 'žádná externí zóna';
$language['DASHBOARD_VISIT_WEBUI'] = 'LMS-WebUI';
$language['DASHBOARD_VISIT_WEBUI_SETTINGS'] = 'LMS-WebUI-Nastavení';
$language['DASHBOARD_TT_PLAY'] = 'Přehrávač hraje';
$language['DASHBOARD_TT_PAUSE'] = 'Přehrávač je pozastaven';
$language['DASHBOARD_TT_STOP'] = 'Přehrávač je zastavený';
$language['DASHBOARD_TT_VISIT_WEBUI'] = 'Prohlédnout LMS-WebUI';
$language['DASHBOARD_TT_VISIT_WEBUI_SETTINGS'] = 'Prohlédnout LMS-WebUI-Nastavení';

//About
$language['ABOUT_PAGE_TITLE'] = 'O - MS4L';
$language["ABOUT_TITLE"] = 'O MusicServer4Lox';
$language["ABOUT_TITLE_COPYRIGHT"] = 'Autorká práva';
$language["ABOUT_COPYRIGHT_MSG"] = '
MusicServer4Lox byl vyvinut (Dieter Schmidberger) a prezentován na loxforum.com.<br>Tento software je nabízen zdarma, ale je chráněn autorským právem.
<br>To znamená, že software lze distribuovat bezplatně, ale změna kódu je povolena pouze s výslovným souhlasem držitele autorských práv.
<br>To se týká pouze částí softwaru, které jsem napsal.';
$language["ABOUT_TITLE_LICENCE"] = 'Licence';
$language["ABOUT_TITLE_DISCLAIMER"] = 'Odpovědnost';
$language["ABOUT_DISCLAIMER_MSG"] = '
Tento software byl vytvořen podle mého nejlepšího vědomí a svědomí mnou a pro mě osobně.
<br>V naději, že je užitečný i pro ostatní, jsem ji zveřejnil.
<br>Za funkčnost neberu žádnou odpovědnost. Používejte software na vlastní nebezpečí!';
$language["SUPPORT_TITLE_CREDITS"] = 'Credits';
$language["SUPPORT_PICTURES"] = 'Obrázky';
$language["SUPPORT_TITLE_CODING"] = 'Kódování';
$language["SUPPORT_TITLE_TRANSLATION"] = 'Překlad';

//Settings Player internen
$language['SET_PLAYER_PAGE_TITLE'] = 'MS4L Nastavení zón';
$language['SET_PLAYER_TITLE'] = 'Nastavení zón (interní)';
$language['SET_PLAYER_TITLE_EXT'] = 'Nastavení zón (externí)';
$language['SET_PLAYER_TITLE_ZONES'] = 'Zóny';
$language['SET_PLAYER_TITLE_ZONE'] = 'Zóna';
$language['SET_PLAYER_TITLE_SOUNDCARD'] = 'Zvuková karta';
$language['SET_PLAYER_TITLE_SQ2LOX'] = 'Sqeeze2Lox';
$language['SET_PLAYER_TITLE_PM'] = 'Správce napájení';
$language['SET_PLAYER_DEL'] = 'Odstranit zónu';
$language['SET_PLAYER_PLAYERNEW'] = 'Nová zóna';
$language['SET_PLAYER_MANUAL'] = 'Zadejte název zóny / Zóna-Mac manuální';
$language['SET_PLAYER_ZONENUMBER'] = 'Číslo zóny';
$language['SET_PLAYER_ZONENAME'] = 'Název zóny';
$language['SET_PLAYER_ZONEMAC'] = 'Mac-Adresa';
$language['SET_PLAYER_ZONEALSA'] = 'Alsa nastavení';
$language['SET_PLAYER_ZONESOUNDCARD'] = 'Zón-zvuková karta';
$language['SET_PLAYER_CH1L'] = 'Kanál 1 levý';
$language['SET_PLAYER_CH1R'] = 'Kanál 1 pravý';
$language['SET_PLAYER_CH2L'] = 'Kanál 2 levý';
$language['SET_PLAYER_CH2R'] = 'Kanál 2 pravý';
$language['SET_PLAYER_CH3L'] = 'Kanál 3 levý';
$language['SET_PLAYER_CH3R'] = 'Kanál 3 pravý';
$language['SET_PLAYER_CH4L'] = 'Kanál 4 levý';
$language['SET_PLAYER_CH4R'] = 'Kanál 4 pravý';
$language['SET_PLAYER_MSG_USE'] = 'Aktivujte bránu aplikace MusicServer';
$language["SET_PLAYER_MSG_ZONENR"] = 'Číslo hudební zóny';
$language['SET_PLAYER_SQ2LOX_USE'] = 'Aktivujte Squeeze2Lox pro zónu';
$language['SET_PLAYER_SQ_VTITITLE'] = 'VTI Umělec/Název';
$language['SET_PLAYER_SQ_VTIMODE'] = 'VTI Režim';
$language['SET_PLAYER_SQ_VIVOLUME'] = 'VI Hlasitost';
$language['SET_PLAYER_SQ_VIPOWER'] = 'VI Napájení';
$language['SET_PLAYER_SQ_VTISYNC'] = 'VTI Synchronizace';
$language['SET_PLAYER_PM_USE'] = 'Aktivivat Správce napájení pro tuto zónu';
$language['SET_PLAYER_PM_VIAMP'] = 'VI Zesilovač';
$language['SET_PLAYER_PM_URLON'] = 'URL - ON';
$language['SET_PLAYER_PM_URLOFF'] = 'URL - OFF';
$language['SET_PLAYER_MODAL_WRITE_TITLE'] = 'Uložit nastavení';
$language['SET_PLAYER_MODAL_WRITEOK_MSG'] = 'Nastavení bylo uloženo.<br>Přehrávač a všechny služby byly restartovány.';
$language['SET_PLAYER_MODAL_WRITENOK_MSG'] = 'Nastavení nelze uložit!';
$language['SET_PLAYER_MODAL_DEL_TITLE'] = 'Smazat zónu';
$language['SET_PLAYER_MODAL_DELOK_MSG'] = 'Zóna byla úspěšně odstraněna.';
$language['SET_PLAYER_MODAL_DELNOK_MSG'] = 'Zóna nemohla být odstraněna.';
$language['SET_PLAYER_MODAL_DELASK_MSG'] = 'Jste si jistý že schcete tuto zónu odstranit?';
$language['SET_PLAYER_REQ_NAME'] = 'Musíte zadat jméno. (A-Z, a-z, 0-9, _, -, Mezery)';
$language['SET_PLAYER_REQ_ALSA'] = 'Tyto parametry jsou povinné. Std. je (80:4::)';
$language['SET_PLAYER_REQ_SC'] = 'Tento kanál je vyžadován.';
$language['SET_PLAYER_REQ_URL'] = 'Zadejte správný formát adresy URL. (http://xxxx.xx)';
$language['SET_PLAYER_TT_NAME'] = 'A-Z a-z 0-9 _ - Mezery';
$language['SET_PLAYER_TT_ALSA'] = 'Změňte pouze v případě problémů.';
$language['SET_PLAYER_TT_FROMSYSTEM'] = 'Přiřazeno systémem.';
$language['SET_PLAYER_TT_CH'] = 'vybrat kanál.';
$language['SET_PLAYER_TT_CHOPT'] = 'Vybrat optimální kanál.';
$language['SET_PLAYER_TT_NUMBERSONLY'] = 'Zadejte pouze čísla.';
$language['SET_PLAYER_TT_URL'] = 'Vstupní formát http://xxxx.xx';
$language['SET_PLAYER_TT_MSG_NUMBER'] = 'Vyberte číslo audio zóny, kterou používáte v Lox-Config pro tuto zónu.';

//User
$language['SET_USER_PAGE_TITLE'] = 'MS4L uživatelské nastavení';
$language['SET_USER_TITLE'] = 'Uživatelské nastavení';
$language['SET_USER_TITLE_USER'] = 'Uživatel';
$language['SET_USER_NAME'] = 'Uživatelské jméno';
$language['SET_USER_PASSWORD'] = 'Heslo';
$language['SET_USER_PASSWORD_CONFIRM'] = 'Potvrzení hesla';
$language['SET_USER_REQ_NAME'] = 'Jméno musí být zadáno.. (A-Z, a-z, 0-9, _, -)';
$language['SET_USER_REQ_PASS'] = 'Musíte zadat heslo. (A-Z, a-z, 0-9, _, ?, , +, -, ., :, -)';
$language['SET_USER_REQ_PASS_CONFIRM'] = 'Heslo musí být stejné jako při zadání výše.';
$language['SET_USER_MODAL_WRITE_TITLE'] = 'Uložit nastavení';
$language['SET_USER_MODAL_WRITEOK_MSG'] = 'Nastavení byla úspěšně uložena.';
$language['SET_USER_MODAL_WRITENOK_MSG'] = 'Nastavení nelze uložit!';

//Equalizer
$language['SET_EQ_PAGE_TITLE'] = 'MS4L-Equalizer';
$language["SET_EQ_TITLE"] = 'Equalizer';
$language["SET_EQ_TITLE_ZONES"] = 'Zóny';
$language["SET_EQ_LOAD_PRESET"] = 'Předvolby';
$language["SET_EQ_LOAD"] = 'Načíst uživatelské nastav.';
$language["SET_EQ_SAVE"] = 'Uložit uživatelské nastav.';
$language["SET_EQ_LOAD_PRESET_FLAT"] = 'Flat';
$language["SET_EQ_LOAD_PRESET_ROCK"] = 'Rock';
$language["SET_EQ_LOAD_PRESET_LOUD"] = 'Loudness';
$language["SET_EQ_LOAD_PRESET_POP"] = 'Pop';
$language["SET_EQ_LOAD_PRESET_CLASSIC"] = 'Classic';
$language["SET_EQ_LOAD_USER1"] = '1';
$language["SET_EQ_SAVE_USER1"] = '1';
$language["SET_EQ_LOAD_USER2"] = '2';
$language["SET_EQ_SAVE_USER2"] = '2';
$language["SET_EQ_LOAD_GLOBAL"] = 'Globalní';
$language["SET_EQ_SAVE_GLOBAL"] = 'Globalní';
$language['SET_EQ_MODAL_WRITE_TITLE'] = 'Uložit nastavení';
$language['SET_EQ_MODAL_WRITEOK_MSG'] = 'Nastavení byla úspěšně uložena.';
$language['SET_EQ_MODAL_WRITENOK_MSG'] = 'Nastavení nelze uložit!';
$language["SET_EQ_MSG"] = 'Pro tuto zónu byl aktivován Music Server Gateway. Nastavte prosím v aplikaci ekvalizér.';

//Setup Soundcard
$language['SET_SC_PAGE_TITLE'] = 'MS4L-Zvuková karta nastavení';
$language["SET_SC_TITLE"] = 'Zvuková karta nastavení';
$language["SET_SC_TITLE_SC"] = 'Zvukové karty';
$language["SET_SC_TITLE_SC_SYSTEM"] = 'Zvukové karty nalezeny';
$language["SET_SC_SC1_USE"] = 'Zvuková karta 1 povolit';
$language["SET_SC_SC2_USE"] = 'Zvuková karta 2 povolit';
$language["SET_SC_SC3_USE"] = 'Zvuková karta 3 povolit';
$language["SET_SC_SC4_USE"] = 'Zvuková karta 4 povolit';
$language["SET_SC_SC5_USE"] = 'Zvuková karta 5 povolit';
$language["SET_SC_SC6_USE"] = 'Zvuková karta 6 povolit';
$language["SET_SC_SC7_USE"] = 'Zvuková karta 7 povolit';
$language["SET_SC_DEVICENR"] = 'Číslo zařízení';
$language["SET_SC_CHANNEL"] = 'Číslo kanálu';
$language["SET_SC_SAMPLING"] = 'Vzorkovací frekvence';
$language['SET_SC_REQ_DEVCIENR'] = 'Číslo zařízení je povinné';
$language['SET_SC_REQ_CHANNEL'] = 'Číslo kanálu je povinné';
$language['SET_SC_REQ_SAMPLING'] = 'Je vyžadována vzorkovací frekvence';
$language['SET_SC_MODAL_WRITE_TITLE'] = 'Uložit nastavení';
$language['SET_SC_MODAL_WRITEOK_MSG'] = 'Nastavení byla úspěšně uložena.';
$language['SET_SC_MODAL_WRITENOK_MSG'] = 'Nastavení nelze uložit!';

//Update
$language['UPDATE_PAGE_TITLE'] = 'MS4L-Aktualizace';
$language["UPDATE_TITLE"] = 'Aktualizace';
$language["UPDATE_TITLE_MUSICSERVER"] = 'MusicServer';
$language["UPDATE_TITLE_LMS"] = 'Logitech Media Server';
$language["UPDATE_TITLE_SQUEEZE"] = 'Squeezelite';
$language["UPDATE_TITLE_SYSTEM"] = 'System';
$language["UPDATE_VERSION_ONLINE"] = 'Verze online';
$language["UPDATE_VERSION_INSTALLED"] = 'Nainstalovaná verze';
$language["UPDATE_VERSION_PACKAGES"] = 'Balíček(y)';
$language["UPDATE_BTN_UPDATE"] = 'Aktualizace';
$language["UPDATE_BTN_LOG"] = 'Log-soubor (poslední aktalizace)';
$language["UPDATE_BTN_RELOAD"] = 'Po zobrazení "ÚSPĚŠNĚ AKTUALIZOVANÉ" stránku můžete znovu načíst stránku';
$language["UPDATE_BTN_CHANGELOG"] = 'Changelog';
$language["UPDATE_MAILER_SUBJECT"] = 'MS4L - Informace o aktualizaci';
$language["UPDATE_MAILER_TEXT"] = 'Pozor {realname},<br><br>pro váš music server jsou aktualizace.<br><br>{verions}<br><br>MusicServer4Lox Team.';

//Backup
$language['BACKUP_PAGE_TITLE'] = 'MS4L-záloha';
$language["BACKUP_TITLE"] = 'Záloha';
$language["BACKUP_TITLE_MUSICSERVER"] = 'MusicServer';
$language["BACKUP_TITLE_MUSICSERVER_SETTINGS"] = 'MusicServer nastavení';
$language["BACKUP_TITLE_LMS"] = 'Logitech Media Server';
$language["BACKUP_TITLE_LMS_FAV"] = 'Logitech Media Server oblíbené';
$language["BACKUP_TITLE_LMS_PL"] = 'Logitech Media Server playlists';
$language["BACKUP_TITLE_MS_FS"] = 'MusicServer soubory a nastavení';
$language["BACKUP_BTN_CHOOSEFILE"] = 'Vyberte soubor';
$language["BACKUP_BTN_BACKUP"] = 'Záloha';
$language["BACKUP_BTN_RESTORE"] = 'Obnovit';
$language["BACKUP_BTN_BACKUPLOG"] = 'Log-File (poslední záloha)';
$language["BACKUP_BTN_RESTORELOG"] = 'Log-File (poslední obnova)';

//Reset Server
$language['RESET_SERVER_PAGE_TITLE'] = 'MS4L Server - resetování';
$language['RESET_SERVER_TITLE'] = 'Server-resetovat';
$language["RESET_SERVER_TITLE_BOX"] = 'Resetovat nastavení serveru';
$language["RESET_SERVER_BTN"] = 'Server reset';
$language["RESET_SERVER_MSG"] = '
<br>Po kliknutí na tlačítko se všechna nastavení MS4L kromě sítě resetují.
<br>Logitech Media Server není resetován!
<br>To lze vrátit pouze obnovením zálohy.';
$language["RESET_SERVER_MSG2"] = 'Nastavení bylo resetováno.<br>systém bude automaticky restartován...';
$language["RESET_SERVER_MODAL_TITLE"] = 'Server-nastavení reset';
$language["RESET_SERVER_MODAL_SHUTDOWN_MSG"] = 'Opravdu chcete resetovat server?';

//SupportData
$language['SUPPORT_PAGE_TITLE'] = 'MS4L Podpora';
$language['SUPPORT_TITLE'] = 'Podpora-Data';
$language["SUPPORT_TITLE_BOX"] = 'Podpora-Data';
$language["SUPPORT_DOWNLOAD_BTN"] = 'Stáhněte si textový soubor pro podporu v Loxforum';

//Template Generator
$language['TEMPLATE_PAGE_TITLE'] = 'MS4L Tvůrce šablon';
$language['TEMPLATE_TITLE'] = 'Tvůrce šablon';
$language["TEMPLATE_TITLE_BOX"] = 'Stáhnout';
$language["TEMPLATE_DOWNLOAD_CLI"] = 'CLI- šablona';
$language["TEMPLATE_DOWNLOAD_T5"] = 'T5 - šablona';
$language["TEMPLATE_DOWNLOAD_TS"] = 'TS - šabloba (alternativní manipulace s T5 (Labmaster)';

//Services
$language['SET_SERVICE'] = 'Služby';
$language['SET_SERVICE_PM'] = 'Správce napájení';
$language['SET_SERVICE_SQ2LOX'] = 'Squeeze2lox / MSG';
$language['SET_SERVICE_FC'] = 'FritzCall-Monitor';
$language['SET_SERVICE_T5'] = 'T5 Receiver';
$language['SET_SERVICE_FM'] = 'FollowMe';
$language['SET_SERVICE_MODAL_WRITE_TITLE'] = 'Uložit nastavení';
$language['SET_SERVICE_MODAL_WRITEOK_MSG'] = 'Nastavení byla úspěšně uložena.';
$language['SET_SERVICE_MODAL_WRITENOK_MSG'] = 'Nastavení nelze uložit!';

//PowerManager
$language['SET_PM_PAGE_TITLE'] = 'MS4L-služba PowerManger';
$language["SET_PM_TITLE"] = 'Správce napájení nastavení';
$language["SET_PM_TITLE_ZONE"] = 'Správce napájení';
$language["SET_PM_TITLE_AMP"] = 'Zesilovač';
$language["SET_PM_TITLE_VOLUMERESET"] = 'Hlasitost reset';
$language['SET_PM_AUTOSTART'] = 'Aktivní autostart';
$language['SET_PM_POWEROFF_STOP'] = 'Po zastavení vypne zónu';
$language['SET_PM_POWEROFF_PAUSE'] = 'Po pauze vypne zónu';
$language['SET_PM_AMPALL'] = 'Aktivujte Amplifier-Control pro všechny zóny';
$language['SET_PM_AMPOFF'] = 'AVypnutí zesilovače po čase';
$language['SET_PM_AMPSTARTUP'] = 'Zapnutí zesilovače po čase';
$language['SET_PM_VIAMPALL'] = 'Zesilovač VI';
$language['SET_PM_URLALLON'] = 'URL - On';
$language['SET_PM_URLALLOFF'] = 'URL - Off';
$language["SET_PM_VOLUMERESET"] = 'Aktivujte interní zóny pro resetování hlasitosti';
$language["SET_PM_STARTVOLUME"]  = 'Hodnota hlasitosti pro interní zóny';
$language["SET_PM_VOLUMERESET_EXT"] = 'Aktivujte externí zóny pro resetování hlasitosti';
$language["SET_PM_STARTVOLUME_EXT"]  = 'Hodnota hlasitosti pro externí zóny';
$language["SET_PM_REQ_POWEROFF"] = 'Zde musí být zadán čas> = 10 sekund.';
$language["SET_PM_REQ_AMPSTARTUP"] = 'Zde musí být zadána hodnota, a to i po dobu 0 sekund.';
$language["SET_PM_REQ_AMPVIAMPALL"] = 'Zde je třeba zadat hodnotu 0, pokud by se měla přepínat pouze adresa URL.';
$language["SET_PM_REQ_STARTVOLUME"] = 'Zde je třeba zadat hodnotu Start-Volume> 0.';
$language["SET_PM_REQ_AMPURL"] = 'Zadejte správný formát adresy URL. (http://xxxx.xx)';
$language['SET_PM_TT_NUMBERSONLY'] = 'Zadejte pouze čísla.';
$language['SET_PM_TT_URL'] = 'Vstupní formát http://xxxx.xx';;

//Squeezw2Lox
$language['SET_SQ2LOX_PAGE_TITLE'] = 'MS4L-Service Squeeze2Lox / MSG';
$language["SET_SQ2LOX_TITLE"] = 'Squeeze2Lox / MSG nastavení';
$language["SET_SQ2LOX_TITLE_BOX"] = 'Squeeze2Lox / MusicServerGateway';
$language['SET_SQ2LOX_AUTOSTART'] = 'Aktivní autautostart';

//T5 Receiver
$language['SET_T5_PAGE_TITLE'] = 'MS4L-Service T5-Receiver';
$language["SET_T5_TITLE"] = 'T5-Receiver nastavení';
$language["SET_T5_TITLE_BOX"] = 'T5-Receiver';
$language['SET_T5_AUTOSTART'] = 'Aktivní  autostart';
$language['SET_T5_UDP'] = 'UDP Port';
$language['SET_T5_SAYFAV'] = 'Řekněte Oblíbené';
$language['SET_T5_SAYFAVVOL'] = 'Řekněte oblíbené pro zvýšení hlasitosti';
$language['SET_T5_VOLUMESTEP'] = 'Krok hlasitosti';
$language["SET_T5_REQ_UDP"] = 'Hodnota musí být 1-65535';
$language["SET_T5_TT_UDP"] = 'UDP port přes který MiniServer odesílá data do MS4L';
$language["SET_T5_TT_VOLUMESTEP"] = 'Kroky hlasitosti - hodnota / krok (kliknutí) zvyšuje / snižuje.';
$language["SET_T5_TT_SAYFAVVOL"] = 'Při oznamování oblíbených položek se hlasitost zvýší o tuto hodnotu.';

//FollowMe
$language['SET_FM_PAGE_TITLE'] = 'MS4L-Služba FollowMe';
$language["SET_FM_TITLE"] = 'FollowMe nastavení';
$language["SET_FM_TITLE_FM"] = 'FollowMe';
$language["SET_FM_STOPFROM"] = 'Stop pro zónu';
$language["SET_FM_PLAYTO"] = 'Play zóna';
$language["SET_FM_VOLUMETO"] = 'Hlasitost ze zóny-> do zóny';
$language["SET_FM_TT_STOPFROM"] = 'Nastavte automatickou zónu na Stop.';
$language["SET_FM_TT_PLAYTO"] = 'Nastavte To-Zone na automatické přehrávání.';
$language["SET_FM_TT_VOLUMETO"] = 'Nastavte stejný hlasitist ze zóny - do zóny.';

//FritzCallMonitor
$language['SET_FC_PAGE_TITLE'] = 'MS4L-služba CallMonitor';
$language["SET_FC_TITLE"] = 'CallMonitor nastavení';
$language["SET_FC_TITLE_FC"] = 'CallMonitor';
$language["SET_FC_TITLE_TTS"]= 'Text2Speech';
$language["SET_FC_TITLE_NUMBERS"]= 'Číslo / jméno';
$language["SET_FC_CMOK"] = 'CallMonitor je dosažitelný.';
$language["SET_FC_CMNOK"] = 'CallMonitor není dosažitelný. Aktivovat?';
$language["SET_FC_CMIPEMPTY"] = 'IP pro Fitzbox není nasteveno.!';
$language["SET_FC_AUTOSTART"]= 'Aktivovat Autostart';
$language["SET_FC_IP"]= 'Fritzbox IP';
$language["SET_FC_USER"]= 'Fritzbox jméno';
$language["SET_FC_PASS"]= 'Fritzbox heslo';
$language["SET_FC_TTSVI"] = 'VI číslo - Start-Impulz';
$language["SET_FC_CALLERVTI"]  = 'VTI číslo - Volající';
$language["SET_FC_CALLEDVTI"] = 'VTI číslo - Volalo';
$language["SET_FC_NUMBERTALKSPEED"] = 'Mluvte telefonní čísla pomalu.';
$language["SET_FC_NUMBER"] = 'Telefoní číslo';
$language["SET_FC_NAME"] = 'Jméno linky';
$language["SET_FC_REQ_IP"] = 'IP musí být zadáno. (xxx.xxx.xxx.xxx)';
$language["SET_FC_REQ_USER"] = 'Jméno musí být zadáno. (A-Z, a-z, 0-9, _, -)';
$language["SET_FC_REQ_PASS"] = 'Heslo musí být zadáno. (A-Z, a-z, 0-9, _, ?, , +, -, ., :, -)';
$language["SET_FC_REQ_TTSVI"] = 'VI pro impuls musí být zadán. (nur Zahlen)';
$language["SET_FC_REQ_CALLERVTI"] = 'VTI volajcí musí být zadán. (pouze číslo)';
$language["SET_FC_REQ_CALLEDVTI"] = 'VTI volajcí musí být zadán. (pouze číslo)';
$language["SET_FC_REQ_NUMBER"] = 'Vložte tel. číslo';
$language["SET_FC_REQ_NAME"] = 'Zadejte jméno, (A-Z, a-z, 0-9, _, -)';

//Network
$language["SET_NETWORK"] = 'Síť';
$language["SET_NETWORK_MOUNT"]  = 'Připojení sdílení';
$language['SET_NET_PAGE_TITLE'] = 'MS4L-Nastavení sítě';
$language["SET_NET_TITLE"] = 'Nastavení sítě';
$language["SET_NET_TITLE_NET"] = 'Síť';
$language["SET_NET_TITLE_HOSTNAME"] = 'Název hostitele';
$language["SET_NET_TITLE_IFACE"] = 'Síťové rozhraní';
$language["SET_NET_IFACENAME"] = 'Rozhraní název';
$language["SET_NET_MACADDRESS"] = 'Mac-adresa';
$language["SET_NET_HOSTNAME"] = 'Název hostitele';
$language["SET_NET_HOSTDOMAIN"] = 'Rozšířené jméno hostitele';
$language["SET_NET_DHCP"] = 'Použít DHCP';
$language["SET_NET_IP"] = 'IP Adresa';
$language["SET_NET_SUBNET"] = 'Podsít';
$language["SET_NET_GATEWAY"] = 'Výchozí brána';
$language["SET_NET_DNSSERVER"] = 'DNS-Server';
$language['SET_NET_MODAL_WRITE_TITLE'] = 'Uložit nastavení';
$language['SET_NET_MODAL_WRITEOK_MSG'] = 'Nastavení byla úspěšně uložena.';
$language['SET_NET_MODAL_WRITEOK_MSG1'] = 'Pro aktivaci nového nastavení je třeba systém restartovat.';
$language['SET_NET_MODAL_WRITEOK_MSG2'] = 'Opravdu chcete restartovat systém?';
$language['SET_NET_MODAL_WRITENOK_MSG'] = 'Nastavení nelze uložit!';

//NetworkShare
$language['SET_NETSHARE_PAGE_TITLE'] = 'MS4L-Nastavení síťový disk'; 
$language["SET_NETSHARE_TITLE"] = 'Síťový disk připojit';
$language["SET_NET_TITLE_NETSHARE"] = 'NAS nastavení';
$language["SET_NET_AUTOMOUNT"] = 'Připojit disk automaticky';
$language["SET_NET_IPHOSTNAME"] = 'IP / Hostname NAS';
$language["SET_NET_FOLDER"] = 'složka NAS';
$language["SET_NET_TITLE_AUTH"] = 'NAS ověření';
$language["SET_NET_LOGIN"] = 'Aktivujte ověření';
$language["SET_NET_USER"] = 'uživatelské jméno';
$language["SET_NET_PASS"] = 'Heslo';
$language["SET_NET_CHARTSET"] = 'Místo iso8859-1 použijte znakovou sadu utf8';
$language["SET_NET_FOLDERNAS"] = 'NAS-složka';
$language["SET_NET_REQ_IPHOSTNAME"] = 'Musí být zadána adresa IP nebo název hostitele (bez //)';
$language["SET_NET_REQ_FOLDER"]  = 'Složka musí být zadána (bez /)';
$language["SET_NET_REQ_USER"] = 'Musíte zadat uživatelské jméno. (A-Z, a-z, 0-9, _, -)';
$language["SET_NET_REQ_PASS"] = 'Musíte zadart heslo (A-Z, a-z, 0-9, _, ?, , +, -, ., :, -)';
$language["SET_NET_BTN_MOUNT"] = 'Připojit disk';
$language["SET_NET_BTN_UNMOUNT"] = 'Odpojit disk';
$language["SET_NET_MOUNTOK"] = 'Síťový disk je připojen';
$language["SET_NET_MOUNTNOK"]  = 'Síťový disk je odpojen';

//Mail
$language['SET_MAIL_PAGE_TITLE'] = 'MS4L-Nastavení eMail'; 
$language["SET_MAIL_TITLE"] = 'eMail nastavení';
$language["SET_MAIL_TITLE_NAME"] = 'eMail / Jméno';
$language["SET_MAIL_TITLE_SMTP"] = 'SMTP-Server';
$language["SET_MAIL_TITLE_UPDATEMAIL"] = 'Aktualizace-Mailer';
$language["SET_MAIL_REALNAME"] = "Skutečné jméno";
$language["SET_MAIL_EMAILTO"] = "eMail-Adresa - Příjemce";
$language["SET_MAIL_SMTPSERVER"] = 'SMTP-Server';
$language["SET_MAIL_SMTPPORT"] = 'SMTP-Port';
$language["SET_MAIL_SMTPSECURE"] = 'SMTP-zabezpečení';
$language["SET_MAIL_SMTPUSER"] = 'SMTP-Uživatelské jméno';
$language["SET_MAIL_SMTPPASS"] = 'SMTP-Heslo';
$language["SET_MAIL_UPDATEMAIL"] = 'Aktivujte Update-Mailer';
$language["SET_MAIL_UPDATEINTERVAL"] = 'Interval aktualizacem mailu';
$language["SET_MAIL_UPDATEDAY"] = 'Aktualizace emailu den';
$language["SET_MAIL_UPDATEWEEKLY"] = 'týdně';
$language["SET_MAIL_UPDATEDAYLY"] = 'dnně';
$language["SET_MAIL_UPDATEMO"] = 'Pondělí';
$language["SET_MAIL_UPDATETU"] = 'Úterý';
$language["SET_MAIL_UPDATEWE"] = 'Středa';
$language["SET_MAIL_UPDATETH"] = 'Čtvrtek';
$language["SET_MAIL_UPDATEFR"] = 'Pátek';
$language["SET_MAIL_UPDATESA"] = 'Sobota';
$language["SET_MAIL_UPDATESU"] = 'Neděle';
$language["SET_MAIL_REQ_REALNAME"] = "Jméno musí být zadáno";
$language["SET_MAIL_REQ_EMAILTO"] = "eMail-adresa musí být zadaná";
$language["SET_MAIL_REQ_SMTPSERVER"] = 'SMTP-Server musí být zadán';
$language["SET_MAIL_REQ_SMTPPORT"] = 'SMTP-Port musí být zadán (1-65535)';
$language["SET_MAIL_REQ_SMTPSECURE"] = 'SMTP-Zabezpečení musí být zadáno';
$language["SET_MAIL_REQ_SMTPUSER"] = 'SMTP-Uživatelské jméno musí být zadáno';
$language["SET_MAIL_REQ_SMTPPASS"] = 'SMTP-Heslo musí být zadáno';
$language["SET_MAIL_BTN_TESTMAIL"] = 'odešli testovací-eMail';
$language["SET_MAIL_TESTOK"] = 'eMail byl úspěšně odeslánt';
$language["SET_MAIL_TESTNOK"] = 'eMail nemohl být odeslán.'; 

//MiniServer
$language['SET_MS_PAGE_TITLE'] = 'MS4L MiniServer Nastavení';
$language['SET_MS_TITLE'] = 'MiniServer nastavení';
$language['SET_MS_TITLE_MS'] = 'MiniServer';
$language['SET_MS_IP'] = 'MiniServer IP-adresa';
$language['SET_MS_USER'] = 'MiniServer Uživatelské jméno';
$language['SET_MS_PASSWORD'] = 'MiniServer Heslo';
$language['SET_MS_PASSWORD_CONFIRM'] = 'MiniServer helso znovu ověření';
$language['SET_MS_REQ_IP'] = 'IP musí být zadaná. (xxx.xxx.xxx.xxx)';
$language['SET_MS_REQ_USER'] = 'Uživatelské jméno musí být zadáno. (A-Z, a-z, 0-9, _, -)';
$language['SET_MS_REQ_PASS'] = 'Heslo musí být zadáno. (A-Z, a-z, 0-9, _, ?, , +, -, ., :, -)';
$language['SET_MS_REQ_PASS_CONFIRM'] = 'Heslo musí být stejné jako při zadání výše.';

//LigitechMediaServer
$language['SET_LMS_PAGE_TITLE'] = 'MS4L Logitech Media Server nastavení';
$language['SET_LMS_TITLE'] = 'Logitech Media Server nastavení';
$language['SET_LMS_TITLE_LMS'] = 'Logitech Media Server';
$language['SET_LMS_IP'] = 'LMS IP-adresa (nastavená systémem)';
$language['SET_LMS_WEBPORT'] = 'LMS WebUI Port';
$language['SET_LMS_CLIPORT'] = 'LMS CLI Port';
$language['SET_LMS_8'] = 'Použít LMS Verze  8 (Beta)';
$language['SET_LMS_REQ_WEBPORT'] = 'Port pro WebUI musí být 1-65535';
$language['SET_LMS_REQ_CLIPORT'] = 'Port pro CLI musí být 1-65535';

//Statisics
$language['SET_STAT_PAGE_TITLE'] = 'MS4L Statistiky nastavení';
$language['SET_STAT_TITLE'] = 'Statistiky';
$language['SET_STAT_TITLE_MS'] = 'Statistiky';
$language['SET_STAT_SEND'] = 'Zasílat statistiky ';
$language['SET_STAT_SHOW'] = 'Ukázat statistiky';
$language['SET_STAT_UUID'] = 'UUID (Univerzálně jedinečný identifikátor)';

//TTS
$language["SET_TTS_TTS"] = 'TTS / CTS';
$language["SET_TTS_RING"] = 'Zvonek/Alarm/Budík';
$language["SET_TTS_WEATHER"] = 'Počasí2TTS';
$language["SET_TTS_TRAVEL"] = 'Dorava2TTS';
$language['SET_TTS_PAGE_TITLE'] = 'MS4L TTS / Zvonění nastavení';
$language['SET_TTS_TITLE'] = 'Text2Speech / Zvonění nastavení';
$language["SET_TTS_TITLE_CACHE"] = 'TTS Mezipamět';
$language["SET_TTS_TITLE_RING"] = 'Zvonek / Alarm / Budík standardní';
$language['SET_TTS_TITLE_DEFAULT'] = 'TTS Standardní';
$language['SET_TTS_DEFAULT_PROVIDER'] = 'Poskytovatel';
$language['SET_TTS_DEFAULT_VOLUME'] = 'Hlasitost';
$language["SET_TTS_DEFAULT_AUTOPLAY"] = 'Pokračovat v přehrýávání po události';
$language["SET_TTS_DEFAULT_AUTOSYNC"] = 'Auto. synchronizace po události';
$language['SET_TTS_DEFAULT_OVERLAY'] = 'Aktivujte přektývání';
$language['SET_TTS_DEFAULT_OVERLAYDROP'] = 'Overlay-Drop';
$language['SET_TTS_DEFAULT_VOLUME'] = 'Hlasitost';
$language['SET_TTS_DEFAULT_TIMEOUT'] = 'Časový limit';
$language["SET_TTS_DEFAULT_RING_FILE"] = 'Vyzvánění';
$language["SET_TTS_DEFAULT_DELETE_FILEAFTER"] = 'Odstraňte nepoužívané soubory TTS (dny)';
$language["SET_TTS_DEFAULT_TTSCACHE"] = 'TTS mezipamět';
$language["SET_TTS_DEFAULT_TTSCACHEFILE"] = 'Soubor(y)';
$language["SET_TTS_DEFAULT_CLEARCACHE"] = 'Smazat TTS - Mezipamět';
$language['SET_TTS_TITLE_POLLY'] = 'Polly';
$language['SET_TTS_TITLE_VOICERSS'] = 'VoiceRSS';
$language['SET_TTS_TITLE_RESPONSIVEVOICE'] = 'Responsive Voice';
$language["SET_TTS_POLLY_ACCESSID"] = 'Přístupový klíč ID';
$language["SET_TTS_POLLY_SECRETKEY"] = 'Tajný přístupový klíč';
$language["SET_TTS_POLLY_LANG"] = 'Jazyk / Hlas';
$language["SET_TTS_VOICERSS_TOKEN"] = 'Token';
$language["SET_TTS_VOICERSS_QUALITY"] = 'Kvalita zvuku';
$language["SET_TTS_VOICERSS_LANG"] = 'Jazyk / Hlas';
$language["SET_TTS_REQ_DEFAULT_VOLUME"] = 'Hlasitost musí být mezi 1-100.';
$language["SET_TTS_REQ_DEFAULT_TIMEOUT"] = 'Časový limit musí být mezi 10-360.';;
$language["SET_TTS_REQ_DEFAULT_DELETE_FILEAFTER"] = 'Dny musí být mezi 0-365.';

//Weather2TTS
$language['SET_W2T_PAGE_TITLE'] = 'MS4L - Počasí2TTS nastavení';
$language["SET_W2T_TITLE"] = 'Počasí2TTS nastavení';
$language["SET_W2T_TITLE_BASIC"] = 'Základní nastavení';
$language["SET_W2T_TITLE_DEFAULT"] = 'Standardní nastavení';
$language["SET_W2T_TITLE_TEXT"] = 'Text';
$language["SET_W2T_DEFAULT_APIKEY"] = 'API-Key - Weatherbit.io';
$language["SET_W2T_DEFAULT_LAT"] = 'Zeměpisná šířka';
$language["SET_W2T_DEFAULT_LON"] = 'Zeměpisná délka';
$language["SET_W2T_DEFAULT_FINDGEO"] = 'Najdi geodata';
$language["SET_W2T_DEFAULT_STANDARD_OUT"] = 'Výstup'; 
$language["SET_W2T_DEFAULT_RAIN_THRESHOLD"] = 'Prahová hodnota větru';
$language["SET_W2T_DEFAULT_WIND_THRESHOLD"] = 'Prahová hodnota deště';
$language["SET_W2T_DEFAULT_TEXT"] = 'Úpravy textů';
$language["SET_W2T_DEFAULT_TEXTCHOOSE"] = 'Zvolte text';
$language["SET_W2T_DEFAULT_OVERWRITE"] = 'Přepsat texty jazykovým standardem';
$language["SET_W2T_TEXT_RAIN"] = 'Déšť (dnes)';
$language["SET_W2T_TEXT_WIND"] = 'Vítr (dnes)';
$language["SET_W2T_TEXT_RAINFC"] = 'Dešť (předpověď)';
$language["SET_W2T_TEXT_WINDFC"] = 'Vítr (předpověď)';
$language["SET_W2T_TEXT_NOW"] = 'Nyní (nyní)';
$language["SET_W2T_TEXT_TODAY"] = 'Dnes (dnes)';
$language["SET_W2T_TEXT_TODAY22"] = 'Dnes po 22 hodině (dnes)';
$language["SET_W2T_TEXT_TODAYTOMORROW"] = 'Dnes a zítra (dnes předpověď)';
$language["SET_W2T_TEXT_FORECAST"] = 'Zítra (předpověď)';
$language["SET_W2T_TEXT_DAYTIME6_11"] = 'mezi 6-11 hodinou (během dne)';
$language["SET_W2T_TEXT_DAYTIME11_17"] = 'mezi 11-17 hodinou (během dne)';
$language["SET_W2T_TEXT_DAYTIME17_22"] = 'mezi 17-22 hodinou (během dne)';
$language["SET_W2T_TEXT_DAYTIME22_6"] = 'mezi 22-6 hodinou (během dne)';
$language["SET_W2T_TEXT_CUSTOM1"] = 'vlastní 1 (vlastní_1)';
$language["SET_W2T_TEXT_CUSTOM2"] = 'vlastní 2 (vlastní_2)';
$language["SET_W2T_TEXT_ASTRONOMIC"] = 'Astro (astronomický)';
$language["SET_W2T_TEXT_SUNSET"] = 'Západ slunce a východ slunce (západ slunce)';
$language["SET_W2T_TEXT_MOONPHASE"] = 'Fáze měsíce (fáze měsíce)';
$language["SET_W2T_REQ_DEFAULT_APIKEY"] = 'API musí být nastaven';
$language["SET_W2T_REQ_DEFAULT_LAT"] = 'Zeměpisná šířka musí být mezi -90 a 180°';
$language["SET_W2T_REQ_DEFAULT_LON"] = 'Zeměpisná délka musí být mezi  -90 a 180°';
$language["SET_W2T_REQ_DEFAULT_RAIN_THRESHOLD"] = 'Prahová hodnota deště musí být mezi 0-100%.';
$language["SET_W2T_REQ_DEFAULT_WIND_THRESHOLD"] = 'Prahová hodnota větru musí být mezi 0-100%.';
//Weather2TTS Standard-Texts
//These Texts can be load as standards in WebUi, and changes can be made in the WebUI. Changes will be saved in the Config-File.
//All words between {} are variables and may not be modified!!!
$language["WTTS_TEXT_RAIN_TODAY"] = 'Pravděpodobnost deště je {chance_of_rain_today} procent.';
$language["WTTS_TEXT_WIND_TODAY"] = 'Fouká {wind_text_today} ze směru {wind_direction_today} s rychlostmi až {wind_speed_today} km/h.';
$language["WTTS_TEXT_RAIN_TOMORROW"] = 'Pravděpodobnost deště je {chance_of_rain_tomorrow} procent.';
$language["WTTS_TEXT_WIND_TOMORROW"] = 'Bude foukat {wind_text_tomorrow} ze směru {wind_direction_tomorrow} s rychlostí až {wind_speed_tomorrow} km/h.';
$language["WTTS_TEXT_NOW"] = 'Nyní je{condition_now}, při {temp_now} stupních. Vlhkost je na {relative_humidity_now} procenta, tlak vzduchu je na {pressure_now} milibar. Vítr fouká {wind_direction_now} rychlostí {wind_speed_now} km / h s občasnými nárazy {wind_gust_now} Km / h. Množství deště je {rain_fall_now} mm na metr čtvereční. UV záření je {uv_radiation_now}.';
$language["WTTS_TEXT_TODAY"] = 'Dnes {condition_today} nejvyšší teplota je {temp_high_today} stupňů, nejnižší teplota je {temp_low_today} stupňů. {rain_text_today} {wind_text_today} ';
$language["WTTS_TEXT_TODAY2"] = 'Nyní {condition_now} je aktuální teplota {temp_now} stupňů. Zítra {condition_tomorrow} a teplota mezi {temp_low_tomorrow} a {temp_high_tomorrow} stupni. {rain_text_tomorrow} {wind_text_tomorrow} ';
$language["WTTS_TEXT_TODAY_FORECAST"] = 'Dnes {condition_today} nejvyšší teplota {temp_high_today} stupňů, nejnižší teplota {temp_low_today} stupňů. {rain_text_today} {wind_text_today}. Zítra {condition_tomorrow} teplota mezi {temp_low_tomorrow} a {temp_high_tomorrow} stupni. {rain_text_tomorrow} {wind_text_tomorrow} ';
$language["WTTS_TEXT_FORECAST"] = 'Zítra má být {condition_tomorrow} teplota mezi {temp_low_tomorrow} a {temp_high_tomorrow} stupňů. {rain_text_tomorrow} {wind_text_tomorrow} ';
$language["WTTS_TEXT_DAYTIME_6_11"] = 'Dnes ráno {condition_now} je očekávaná maximální teplota {temp_high_today} stupňů, aktuální teplota je {temp_now} stupňů. {rain_text_today} {wind_text_today} ';
$language["WTTS_TEXT_DAYTIME_11_17"] = 'Dnes odpoledne {condition_now}. Venkovní teplota  {temp_now} stupňů. {rain_text_today} {wind_text_today} ';
$language["WTTS_TEXT_DAYTIME_17_22"] = 'Ve večerních hodinách {condition_now}. Venkovní teplota {temp_now} stupňů, očekávaná nejnižší teplota dnes večer je {temp_low_today} stupňů. {rain_text_today} {wind_text_today} ';
$language["WTTS_TEXT_DAYTIME_22"] = 'Nyní je {temp_now} stupňů. Zítra {condition_tomorrow} teplota mezi {temp_low_tomorrow} a {temp_high_tomorrow} stupni. {rain_text_tomorrow} {wind_text_tomorrow} ';
$language["WTTS_TEXT_CUSTOM_1"] = 'Tvůj text 1';
$language["WTTS_TEXT_CUSTOM_2"] = 'Tvůj text 2';
$language["WTTS_TEXT_ASTRO"] = 'Východ slunce je dnes v {sunrise}: {sunrise}, západ slunce v {sunseth}: {sunsetm},fáze měsíce je {moonphase}, osvětlení je {moonillu}% .. ';
$language["WTTS_TEXT_SUNSET"] = 'Východ slunce je dnes {sunrise}: {sunrise}, západ slunce {sunset: {sunsetm}. ';
$language["WTTS_TEXT_MOONPHASE"] = 'Měsíc je ve fázi {moonphase}, skutečné osvětlení je {moonillu}%.';

$language ["WTTS_CONDITION_200"] = 'bouřka se slabým deštěm';
$language ["WTTS_CONDITION_201"] = 'bouřka s deštěm';
$language ["WTTS_CONDITION_202"] = 'bouřka se silným deštěm';
$language ["WTTS_CONDITION_230"] = 'bouřka se slabým mrholením';
$language ["WTTS_CONDITION_231"] = 'bouřka s mrholením';
$language ["WTTS_CONDITION_232"] = 'bouřka se silným mrholením';
$language ["WTTS_CONDITION_233"] = 'bouřka s krupobitím';
$language ["WTTS_CONDITION_300"] = 'drobné mrholení';
$language ["WTTS_CONDITION_301"] = 'mrholení';
$language ["WTTS_CONDITION_302"] = 'silné mrholení';
$language ["WTTS_CONDITION_500"] = 'slabý déšť';
$language ["WTTS_CONDITION_501"] = 'mírný déšť';
$language ["WTTS_CONDITION_502"] = 'hustý déšť';
$language ["WTTS_CONDITION_511"] = 'mrznoucí déšť';
$language ["WTTS_CONDITION_520"] = 'lehká přeháňka';
$language ["WTTS_CONDITION_521"] = 'přeháňka';
$language ["WTTS_CONDITION_522"] = 'silná přeháňka';
$language ["WTTS_CONDITION_600"] = 'slabé sněžení';
$language ["WTTS_CONDITION_601"] = 'sněží';
$language ["WTTS_CONDITION_602"] = 'silně sněží';
$language ["WTTS_CONDITION_610"] = 'mix sněžení/ déšťě';
$language ["WTTS_CONDITION_611"] = 'plískanice';
$language ["WTTS_CONDITION_612"] = 'těžká plískanice';
$language ["WTTS_CONDITION_621"] = 'chumelenice ';
$language ["WTTS_CONDITION_622"] = 'silná chumelenice';
$language ["WTTS_CONDITION_623"] = 'sněhová přeháňka';
$language ["WTTS_CONDITION_700"] = 'mlha';
$language ["WTTS_CONDITION_711"] = 'smog';
$language ["WTTS_CONDITION_721"] = 'opar';
$language ["WTTS_CONDITION_731"] = 'písek / prach';
$language ["WTTS_CONDITION_741"] = 'mlhu';
$language ["WTTS_CONDITION_751"] = 'mrznoucí mlhu';
$language ["WTTS_CONDITION_800"] = 'jasno';
$language ["WTTS_CONDITION_801"] = 'skoro jasno';
$language ["WTTS_CONDITION_802"] = 'polojasno';
$language ["WTTS_CONDITION_803"] = 'oblačno';
$language ["WTTS_CONDITION_804"] = 'zataženo';
$language ["WTTS_CONDITION_900"] = 'neznámé srážky';

$language["WTTS_DIRECTION_UKNOWN"] = 'nedefinováno';
$language["WTTS_DIRECTION_E"] = 'Východní';
$language["WTTS_DIRECTION_ENE"] = 'Východoseverovýchodí';
$language["WTTS_DIRECTION_ESE"] = 'Východní jihovýchod';
$language["WTTS_DIRECTION_N"] = 'Severní';
$language["WTTS_DIRECTION_NE"] = 'Severovýchodní';
$language["WTTS_DIRECTION_NNE"] = 'Severní severovýchod';
$language["WTTS_DIRECTION_NNW"] = '	Severo-severozápadní';
$language["WTTS_DIRECTION_NW"] = 'Severozápadní';
$language["WTTS_DIRECTION_S"] = 'Jižní';
$language["WTTS_DIRECTION_SE"] = 'Jihovýchodní';
$language["WTTS_DIRECTION_SSE"] = 'Jihovýchodní';
$language["WTTS_DIRECTION_SSW"] = 'Jihozápadní';
$language["WTTS_DIRECTION_SW"] = 'Jihozápadnít';
$language["WTTS_DIRECTION_W"] = 'Západní';
$language["WTTS_DIRECTION_WNW"] = 'Západ-severozápadní';
$language["WTTS_DIRECTION_WSW"] = 'Západ-jihozápadní';
//Beaufort scale
$language["WTTS_WIND_1"] = 'vánek';
$language["WTTS_WIND_2"] = 'větřík';
$language["WTTS_WIND_3"] = 'slabý vítr';
$language["WTTS_WIND_4"] = 'dosti čerství vítr';
$language["WTTS_WIND_5"] = 'čerství vítr';
$language["WTTS_WIND_6"] = 'silná vítr';
$language["WTTS_WIND_7"] = 'prudký vítr';
$language["WTTS_WIND_8"] = 'bouřlivý vítr';
$language["WTTS_WIND_9"] = 'vichřice';
$language["WTTS_WIND_10"] = 'silná vychřice';
$language["WTTS_WIND_11"] = 'mohutná vychřice';
$language["WTTS_WIND_12"] = 'orkán';
      
$language["WTTS_MOON_1"] = 'Nový měsíc';
$language["WTTS_MOON_2"] = 'Dorůstajcí srpek';
$language["WTTS_MOON_3"] = 'První čtvrt';
$language["WTTS_MOON_4"] = 'Dorůstajcí měsíc';
$language["WTTS_MOON_5"] = 'Úplněk';
$language["WTTS_MOON_6"] = 'Couvajcí měsíc';
$language["WTTS_MOON_7"] = 'Poslední čtvrt';
$language["WTTS_MOON_8"] = 'Ubývajicí srpek';

//CTS
$language["CTS_HOUR_1"] = ''; //ein Uhr
$language["CTS_MINUTE_1"] = '';
$language["CTS_MINUTES"] = '';
$language["CTS_TEXT"] = 'Nyní je {hours_12}:{minutes_12} {am_pm}.';

//Traveltiime2TTS
$language['TT2T_PAGE_TITLE'] = 'MS4L - Traveltime2TTS nastavení';
$language["TT2T_TITLE"] = 'Traveltime2TTS nastavení';
$language["TT2T_TITLE_BASIC"] = 'Základní nastavení';
$language["TT2T_TITLE_DEFAULT"] = 'Standartní nastavení';
$language["TT2T_TITLE_TEXT"] = 'Text';
$language["TT2T_DEFAULT_APIKEY"] = 'API-Key - Google';
$language["TT2T_DEFAULT_TRAFFIC"] = 'Zvažte plynulost dopravy'; 
$language["TT2T_DEFAULT_TRAFFICCALC"] = 'Výpočet dopravního provozu';
$language["TT2T_DEFAULT_OVERWRITE"] = 'Přepsat texty jazykovým standardem';
$language["TT2T_TEXT"] = 'Úprava textu pro čas cesty';
$language["TT2T_REQ_DEFAULT_APIKEY"] = 'API musí být nastaveno';
$language["TT2T_TEXT"] = 'Od {start} do {arrival} se {distance} km, bude třeba {duration}.';

//EventCreator
$language["EC_EVNETS"] = 'Události';
$language["EC_TTS"] = 'Text2Speech';
$language["EC_CTS"] = 'Clock2Speech';
$language["EC_RING"] = 'Zvonek/Budík/Alarm';
$language["EC_WEATHER"] = 'Weather2Speech';
$language["EC_TRAVEL"] = 'Traveltime2Speech';
$language['EC_PAGE_TITLE'] = 'MS4L - Tvorba událostí';
$language["EC_TITLE"] = 'Tvorba událostí';
$language["EC_TITLE_VQ_ADDRESS"] = 'Virtuální výstupní adresa';
$language["EC_TITLE_VCQ_COMMANDON"] = 'Virtuální výstup příkaz "Command for ON"';
$language["EC_TITLE_TTS"] = 'Text2Speech';
$language["EC_TITLE_TTS_TEXT"] = 'TTS-Text';
$language["EC_TITLE_CTS_TEXT"] = 'Text po oznámení času';
$language["EC_TITLE_W2T_TODO"] = 'Předpověď počasí ToDo';
$language["EC_TITLE_TT2T"] = 'Dopravní detaily';
$language["EC_TT2T_FROM"] = 'Z adresy';
$language["EC_TT2T_TO"] = 'Na adresu';
$language["EC_TT2T_TRAFFIC"] = 'Zvažte hustotu provozu';
$language["EC_TT2T_TRAFFICCALC"] = 'Výpočet hustoty dopravy';
$language["EC_TT2T_DEPARTURE_TIME"] = 'Čas odjezdu (24h)';
$language["EC_TITLE_RING_FILE"] = 'Soubor';
$language["EC_TITLE_ZONEINTERN"] = 'Zóna interní';
$language["EC_TITLE_ZONEEXTERN"] = 'Zóna externí';
$language["EC_TITLE_OPTIONSBASIC"] = 'Základní';
$language["EC_TITLE_OPTIONSAFTER"] = 'Po události';
$language["EC_TITLE_OPTIONOVERLAY"] = 'Překrytí';
$language["EC_TITLE_OPTIONSPECIAL"] = 'Speciální';
$language["EC_ALL_ZONEINTERN"] = 'Včechny interní zóny';
$language["EC_ALL_ZONEEXTERN"] = 'Všechny externí zóny';
$language["EC_VOLUME"] = 'Hlasitost';
$language["EC_REPEAT"] = 'Opakování události';
$language["EC_SIGNAL"] = 'Signál';
$language["EC_TIMEOUT"] = 'Časový limit';
$language["EC_REIMP"] = 'Připravený pro impulz';
$language["EC_AUTOPLAY"] = 'automatické přehrávání';
$language["EC_SYNC"] = 'Synchronizace';
$language["EC_REPEAT_RESET"] = 'opakujte reset';
$language["EC_OVERLAY"] = 'Překrytí';
$language["EC_OVERLAYDROP"] = 'Překrytí a ztišen';
$language["EC_PRIO"] = 'Priorita';
$language["EC_QUEUE"] = 'Fronta';
$language["EC_SYSTEM_STANDARD"] = 'Používá se systémový standart';
$language["EC_NOSIGNAL"] = 'Nehraje - žáddný signál';
$language["EC_GENERATE"] = 'Generujte události';
$language["EC_REQ_TEXT"] = 'Musíte zadat text.';
$language["EC_REQ_VOLUME"] = 'Hlasitost musí být  1 až 100.';
$language["EC_REQ_TIMEOUT"] = 'Hodnota musí být větší než 10.';
$language["EC_REQ_REIMP"] = 'Hodnota musí být větší než 1.';
$language["EC_REQ_ADDRESS"] = 'Je třeba zadat adresu';
$language["EC_REQ_DEPARTURE_TIME"] = 'Formát času musí být h: m (hodiny: minuty)';
$language["EC_PH_DEPARTURE_TIME"] = 'h:m (prázdný = aktuální čas (nyní))';
$language["EC_STOP"] = 'Stop';

//Zone-Tester
?>
